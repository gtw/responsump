/*
 * responsump, SUMP logic analyser for rESPonse hardware
 *
 * Copyright (C) 2022 Gary Wong
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <driver/gpio.h>
#include <driver/usb_serial_jtag.h>
#include <esp_vfs_dev.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <soc/gpio_struct.h>
#include <time.h>
#include <unistd.h>

// Define to 1 to force USB bus reset before acquisition
#define FORCE_RESET 0
// Define to 1 to abbreviate long runs of consecutive samples
#define IDLE_ELISION 0

#define CMD_RESET 0x00
#define CMD_RUN 0x01
#define CMD_ID 0x02
#define CMD_METADATA 0x04
#define CMD_XON 0x11
#define CMD_XOFF 0x13
#define CMD_SET_DIVIDER 0x80
#define CMD_SET_READ_DELAY 0x81
#define CMD_SET_FLAGS 0x82
#define CMD_TRIGGER_MASK 0xC0
#define CMD_TRIGGER_VAL 0xC1
#define CMD_TRIGGER_CONFIG 0xC2

static unsigned run;
static unsigned xon;
static unsigned divider;
static unsigned readcount, delaycount;
static unsigned flags;
static unsigned tmask, tval, tconfig;
static unsigned acquired;

#define BUF_SIZE 0x20000

static unsigned char buf[ BUF_SIZE ];

static void acquire( int n, int keep ) {

    unsigned int val, prev = 0;
    unsigned char *filled = buf, *p;
    unsigned int advance;
    unsigned int diff;

    while( filled < buf + n ) {
	p = filled;
	val = 0;
	advance = keep;

	// Loop iteration must complete in exactly 16 cycles for 10 MHz rate
	asm volatile(
	    "\nL%=:\n\t"
	    "lbu %[val], 0(%[gpio])\n\t"
	    "addi %[p], %[p], 1\n\t"
	    "sb %[val], -1(%[p])\n\t"
	    "xor %[diff], %[val], %[prev]\n\t"
	    "or %[advance], %[advance], %[diff]\n\t"
	    "mv %[prev], %[val]\n\t"
	    "nop\n\t"
	    "blt %[p], %[end], L%=\n\t"
	    : [p] "+&r" (p), [val] "=&r" (val), [advance] "+&r" (advance),
	      [diff] "=&r" (diff), [prev] "+&r" (prev)
	    : "[p]" "r" (p), [end] "r" (p + 0x100), [gpio] "r" (&GPIO.in.val),
	      "[advance]" "r" (advance), "[prev]" "r" (prev)
	    : "memory" );

	if( advance )
	    filled += 0x100;
    }
}

static void usb_reset( void ) {

    int i;

    // Force long single-ended 0 on channels 0/1
		
    GPIO.pin[ 0 ].val = 0x04;
    GPIO.pin[ 1 ].val = 0x04;

    GPIO.out_w1tc.val = 0x03;
    GPIO.enable_w1ts.val = 0x03;
		
    for( i = 0; i < 0x100000; i++ )
	asm volatile( "nop\n\t" );
    
    GPIO.enable_w1tc.val = 0x03;
}

static void reset( void ) {

    run = 0;
    xon = 1;
    tmask = tval = tconfig = 0;
    divider = 0;
    readcount = delaycount = 0;
    acquired = 0;
}

static unsigned read32( unsigned char *p ) {

    return p[ 0 ] | ( p[ 1 ] << 8 ) | ( p[ 2 ] << 16 ) | ( p[ 3 ] << 24 );
}

extern void app_main( void ) {

    static usb_serial_jtag_driver_config_t cfg = {
	.rx_buffer_size = 0x80,
	.tx_buffer_size = 0x80
    };

    static gpio_config_t gpio_in = {
	.mode = GPIO_MODE_INPUT,
	.pin_bit_mask = 0xFF
    };

    usb_serial_jtag_driver_install( &cfg );

    esp_vfs_usb_serial_jtag_use_driver();
    
    gpio_config( &gpio_in );

    reset();
    
    while( 1 ) {
	static esp_cpu_ccount_t t0 = 0;
	esp_cpu_ccount_t t;

	while( ( ( t = esp_cpu_get_ccount() ) & 0xF0000000 ) ==
	       ( t0 & 0xF0000000 ) ) {
	    static char metadata[] =
		"\x01responsump v1.0\x00" // device name
		"\x21\x00\x02\x00\x00" // sample memory available 128 KB
		"\x23\x00\x98\x96\x80" // maximum sample rate 10 MHz
		"\x40\x08" // number of usable probes 8
		"\x41\x02" // protocol version 2
		"\x00";
	    unsigned char cmd;
	    unsigned char param[ 4 ];
	    
	    if( usb_serial_jtag_read_bytes( &cmd, 1, 0 ) == 1 )
		switch( cmd ) {
		case CMD_RESET:
		    reset();
		    break;
		    
		case CMD_RUN:
		    run = 1;
		    break;
		    
		case CMD_ID:
		    fputs( "1ALS", stdout );
		    fflush( stdout );
		    break;
		    
		case CMD_METADATA:
		    fwrite( metadata, sizeof metadata, 1, stdout );
		    fflush( stdout );
		    break;
		    
		case CMD_XON:
		    xon = 1;
		    break;
		    
		case CMD_XOFF:
		    xon = 0;
		    break;
		    
		case CMD_SET_DIVIDER:
		    usb_serial_jtag_read_bytes( param, sizeof param, 1000 );
		    divider = read32( param );
		    break;
		    
		case CMD_SET_READ_DELAY:
		    usb_serial_jtag_read_bytes( param, sizeof param, 1000 );
		    readcount = ( param[ 0 ] | ( param[ 1 ] << 8 ) ) << 2;
		    delaycount = ( param[ 2 ] | ( param[ 3 ] << 8 ) ) << 2;

		    if( readcount > BUF_SIZE )
			readcount = BUF_SIZE;
		    
		    break;
		    
		case CMD_SET_FLAGS:
		    usb_serial_jtag_read_bytes( param, sizeof param, 1000 );
		    flags = read32( param );
		    break;
		    
		case CMD_TRIGGER_MASK:
		    usb_serial_jtag_read_bytes( param, sizeof param, 1000 );
		    tmask = read32( param );
		    break;
		    
		case CMD_TRIGGER_VAL:
		    usb_serial_jtag_read_bytes( param, sizeof param, 1000 );
		    tval = read32( param );
		    break;
		    
		case CMD_TRIGGER_CONFIG:
		    usb_serial_jtag_read_bytes( param, sizeof param, 1000 );
		    tconfig = read32( param );
		    break;
		    
		}

	    if( run && ( GPIO.in.data & 0xFF & tmask ) == tval ) {
		if( FORCE_RESET )
		    usb_reset();
		
		acquire( readcount, !IDLE_ELISION );
		acquired = readcount;		
		run = 0;
	    }
	    
	    if( xon && acquired ) {
		int n = acquired - 64;

		if( n < 0 )
		    n = 0;

		while( acquired > n )
		    putchar( buf[ --acquired ] );
		
		fflush( stdout );
	    }
	}

	t0 = t;
	vTaskDelay( 1 );		
    }
}
